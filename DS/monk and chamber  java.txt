import java.util.*;
 
class TestClass {
    public static void main(String args[] ) throws Exception {
	       
		 Scanner sc=new Scanner(System.in);
	     int n=sc.nextInt(); //N of spider
	     int k=sc.nextInt(); // X iterations
	     Spider b,max=null;
	     Queue<Spider> original= new LinkedList<Spider>() ;
	     Queue<Spider> choose= new LinkedList<Spider>() ;
	     
	     
	        for(int i=0;i<n;i++){
	            original.add(new Spider(sc.nextInt(),i+1));
	        }
	        
	        int K=k;
	        for(int j=0;j<K;j++)
	        {
	            if(K>original.size())
	            k=original.size();
	            max=null;
	            for(int i=0;i<k;i++)
	            {
	                b=original.poll();
	                if(max==null)
	                     max=b;
	                else
	                {
	                    if(b.getPower()>max.getPower())
	                        max=b;
	                }
	                choose.add(b);
	            }
	            for(int i=0;i<k;i++)
	            {
	                b=choose.poll();
	                if(b.equals(max))
	                    System.out.print(b.getPosition()+" ");
	                else
	                {
	                    b.changePower();;
	                    original.add(b);
	                }
	            }
	        }
 
    }
    
    public static class Spider{
        public int position;
        public int power;
        public Spider(int x,int y)
        {
            power=x;
            position=y;
        }
        
        public int getPosition(){
            return position;
        }
        
        public int getPower(){
            return power;
        }
        
        public void changePower(){
            if(power>0){
                power--;
            }
        }
    }
}