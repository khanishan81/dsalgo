
// C++ program to find sum of all minimum and maximum 
// elements Of Sub-array Size k. 
import java.util.*;

class dqueue {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, k;

        System.out.println("Enter N");
        n = sc.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        k = 3; // window size ;
        int s = calculateSum(arr, n, k);
        System.out.println("Sum = " + s);
    }

    public static int calculateSum(int arr[], int n, int k) {
        Deque<Integer> g = new LinkedList<Integer>();
        Deque<Integer> s = new LinkedList<Integer>();
        int sum = 0;
        int i;
        for (i = 0; i < k; i++) {

            while (!g.isEmpty() && arr[g.getLast()] <= arr[i]) {
                g.removeLast();
            }
            while (!s.isEmpty() && arr[s.getLast()] >= arr[i]) {
                s.removeLast();
            }

            g.addLast(i);
            s.addLast(i);
        }

        for (; i < arr.length; i++) {
            sum = sum + (arr[g.getFirst()] + arr[s.getFirst()]);

            // remove elements out of the windows
            while (!s.isEmpty() && s.getFirst() <= i - k) {
                s.removeFirst();
            }
            while (!g.isEmpty() && g.getFirst() <= i - k) {
                g.removeFirst();
            }

            while (!g.isEmpty() && arr[g.getLast()] <= arr[i]) {
                g.removeLast();
            }
            while (!s.isEmpty() && arr[s.getLast()] >= arr[i]) {
                s.removeLast();
            }

            g.addLast(i);
            s.addLast(i);
        }
        sum = sum + (arr[g.getFirst()] + arr[s.getFirst()]);

        return sum;
    }

}
