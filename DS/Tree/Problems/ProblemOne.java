import java.util.*;


// HACKEREARTH MONK AND FIRE - TREE


class ProblemOne {

    public static BstNode insertbst(BstNode r, int d) {
        if (r == null) {
            BstNode newnode = new BstNode(d);
            r = newnode;
            return newnode;
        }   
        if (d < r.data) {
            r.left = insertbst(r.left, d);
        } else if (d > r.data) {
            r.right = insertbst(r.right, d);
        }
        return r;
    }

    public static int depthoftree(BstNode root){
            if (root == null) {
                return 0;
            }
            int rd = depthoftree(root.right);
            int ld = depthoftree(root.left);
            if (rd > ld) {
                return (1 + rd);
            } else {
                return (1 + ld);
            }
    }
    
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n= sc.nextInt(); 
        BstNode root= null; 
        
        for (int i = 0; i < n; i++) {
            int temp= sc.nextInt();     
            root = insertbst(root, temp);
        }

        System.out.println(depthoftree(root) );
       
    }
}

class BstNode {
    int data;
    BstNode left;
    BstNode right;

    BstNode(int x) {
        this.data = x;
        this.left = null;
        this.right = null;
    }

    public int getData() {
        return data;
    }

    public BstNode getLeft() {
        return left;
    }

    public BstNode getRight() {
        return right;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setLeft(BstNode left) {
        this.left = left;
    }

    public void setRight(BstNode right) {
        this.right = right;
    }
}