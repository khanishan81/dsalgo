import java.util.*;
import java.util.Map.Entry;

class Solution{ 
    
    public static void topView(BstNode r){
       Queue<QueueObj> q = new LinkedList<QueueObj>(); 
       Map<Integer,BstNode>  map= new TreeMap<Integer,BstNode>();
       
       if(r == null){
           return;
       }else {
           q.add(new QueueObj(r,0));
       }

       while (!q.isEmpty()) {
           QueueObj tempnode = q.poll(); 
           
           if(!map.containsKey(tempnode.hd)){
               map.put(tempnode.hd, tempnode.node);
           }

           if(tempnode.node.left != null){
              q.add(new QueueObj(tempnode.node.left,tempnode.hd - 1)); 
           }

           if (tempnode.node.right != null) {
            q.add(new QueueObj(tempnode.node.right,tempnode.hd + 1)); 
           }
       }

       /*   for (Entry<Integer, Node> entry : map.entrySet()) { 
        System.out.print(entry.getValue().data); 
        }  */
         
        System.out.println("TOp view");
        Set<Entry<Integer,BstNode>> entries = map.entrySet();
for (Iterator<Entry<Integer,BstNode>> i = entries.iterator(); i.hasNext(); ) {
    Entry<Integer,BstNode> e = (Entry) i.next(); // allowed
    System.out.print(e.getValue().data +" ");
}
    
    }
    
    public static BstNode insert(BstNode root, int data) {
        if(root == null) {
            return new BstNode(data);
        } else {
            BstNode cur;
            if(data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        BstNode root = null;
        while(t-- > 0) {
            int data = scan.nextInt();
            root = insert(root, data);
        }
        scan.close();
        topView(root);
    }
}

class QueueObj { 
    BstNode node; 
    int hd; 

    QueueObj(BstNode node, int hd) { 
        this.node = node; 
        this.hd = hd; 
    } 
} 


class BstNode {
    int data;
    BstNode left;
    BstNode right;

    BstNode(int x) {
        this.data = x;
        this.left = null;
        this.right = null;
    }

    public int getData() {
        return data;
    }

    public BstNode getLeft() {
        return left;
    }

    public BstNode getRight() {
        return right;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setLeft(BstNode left) {
        this.left = left;
    }

    public void setRight(BstNode right) {
        this.right = right;
    }
} 