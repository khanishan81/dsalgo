import java.util.*;
import java.io.*;

class Node {
    Node left;
    Node right;
    int data;
    
    Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }
}

class LCA {

	/*
    class Node 
    	int data;
    	Node left;
    	Node right;
	*/
	public static Node lca(Node root, int v1, int v2) { 
         List<Integer> path1 = new ArrayList<>(); 
         List<Integer> path2 = new ArrayList<>(); 
         path1.clear(); 
         path2.clear(); 
         int nodedata= findLCAInternal(root, v1, v2,path1,path2); 
         Node result = new Node(nodedata); 
         return result;
    }
   
    public static int findLCAInternal(Node root, int n1, int n2,List<Integer> path1,List<Integer> path2) { 
  
        if (!findPath(root, n1, path1) || !findPath(root, n2, path2)) { 
            System.out.println((path1.size() > 0) ? "n1 is present" : "n1 is missing"); 
            System.out.println((path2.size() > 0) ? "n2 is present" : "n2 is missing"); 
            return -1; 
        } 
  
        int i; 
        for (i = 0; i < path1.size() && i < path2.size(); i++) { 
              
        // System.out.println(path1.get(i) + " " + path2.get(i)); 
            if (!path1.get(i).equals(path2.get(i))) 
                break; 
        } 
        return path1.get(i-1); 
    }
    public static boolean findPath(Node root, int n, List<Integer> path) 
    { 
        // base case 
        if (root == null) { 
            return false; 
        }        
        // Store this node . The node will be removed if 
        // not in path from root to n. 
        path.add(root.data);   
        if (root.data == n) { 
            return true; 
        } 
        if (root.left != null && findPath(root.left, n, path)) { 
            return true; 
        } 
        if (root.right != null && findPath(root.right, n, path)) { 
            return true; 
        }  
        // If not present in subtree rooted with root, remove root from 
        // path[] and return false 
        path.remove(path.size()-1); 
        return false; 
    }  

	public static Node insert(Node root, int data) {
        if(root == null) {
            return new Node(data);
        } else {
            Node cur;
            if(data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        Node root = null;
        while(t-- > 0) {
            int data = scan.nextInt();
            root = insert(root, data);
        }
      	int v1 = scan.nextInt();
      	int v2 = scan.nextInt();
        scan.close();
        Node ans = lca(root,v1,v2);
        System.out.println(ans.data);
    }	
}