import java.util.LinkedList;
import java.util.Queue;
import java.util.*;

// Binary Tree
public class TreeDS {

    // Method to insert new element using level order.
    public static void insertElement(BtNode r, int data) {
        BtNode newnode = new BtNode(data);
        Queue<BtNode> q = new LinkedList<BtNode>();
        q.add(r);
        while (!q.isEmpty()) {
            BtNode el = q.peek();
            q.remove();

            if (el.left == null) {
                el.left = newnode;
                break;
            } else {
                q.add(el.left);
            }

            if (el.right == null) {
                el.right = newnode;
                break;
            } else {
                q.add(el.right);
            }
        }
    }

    // #region [Color1] Tree traversal
    public static void preorder(BtNode r) {
        if (r == null) {
            return;
        }
        System.out.print(r.data + " ");
        preorder(r.left);
        preorder(r.right);
        System.out.println();
    }

    public static void inorder(BtNode r) {
        if (r == null) {
            return;
        }
        inorder(r.left);
        System.out.print(r.data + " ");
        inorder(r.right);

    }

    public static void levelordertraversal(BtNode r) {

        Queue<BtNode> q = new LinkedList<BtNode>();
        q.add(r);

        while (!q.isEmpty()) {
            BtNode curr = q.poll();
            System.out.print(curr.data + " ");

            if (curr.left != null) {
                q.add(curr.left);
            }

            if (curr.right != null) {
                q.add(curr.right);
            }
        }

    }

    // #endregion Tree Traversal end

    public static int sizeoftree(BtNode r) {
        int lc = r.left == null ? 0 : sizeoftree(r.left);
        int rc = r.right == null ? 0 : sizeoftree(r.right);
        return 1 + lc + rc;

    }

    public static int depthoftree(BtNode r) {
        if (r == null) {
            return 0;
        }
        int rd = depthoftree(r.right);
        int ld = depthoftree(r.left);
        if (rd > ld) {
            return (1 + rd);
        } else {
            return (1 + ld);
        }
    }
    // TODO: Calculate Minimum height of tree

    // #region [Color2] for Deleting node from tree

    public static void deletenode(BtNode r, int d) {
        Queue<BtNode> q = new LinkedList<BtNode>();
        q.add(r);

        BtNode keynode = null, temp = null;
        while (!q.isEmpty()) {
            temp = q.peek();
            q.remove();

            if (temp.data == d) {
                keynode = temp;
            }

            if (temp.left != null) {
                q.add(temp.left);
            }
            if (temp.right != null) {
                q.add(temp.right);
            }
        }

        int x = temp.data;
        deletedeepest(r, temp);
        keynode.data = x;
    }

    public static void deletedeepest(BtNode r, BtNode last) {
        Queue<BtNode> q = new LinkedList<BtNode>();
        q.add(r);
        BtNode curr;
        while (!q.isEmpty()) {
            curr = q.peek();
            q.remove();

            if (curr.right == last) {
                curr.right = null;
                return;
            } else {
                q.add(curr.right);
            }

            if (curr.left == last) {
                curr.left = null;
                return;
            } else {
                q.add(curr.left);
            }
        }
    }

    // #endregion NODE Deletion Ends

    // Calculate maximum width of a tree
    public static int maxwidth(BtNode root) {
        // Base case
        if (root == null)
            return 0;

        // Initialize result
        int maxwidth = 0;

        // Do Level order traversal keeping
        // track of number of nodes at every level
        Queue<BtNode> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()) {
            // Get the size of queue when the level order
            // traversal for one level finishes
            int count = q.size();

            // Update the maximum node count value
            maxwidth = Math.max(maxwidth, count);

            // Iterate for all the nodes in
            // the queue currently
            while (count-- > 0) {
                // Dequeue an node from queue
                BtNode temp = q.remove();

                // Enqueue left and right children
                // of dequeued node
                if (temp.left != null) {
                    q.add(temp.left);
                }
                if (temp.right != null) {
                    q.add(temp.right);
                }
            }
        }
        return maxwidth;
    }

    // #region [Color3] for Construction Tree from Inorder and Preorder Traversal

    public static BtNode buildBttree(int[] preorder, int pst, int pend, int[] inorder, int inst, int inend) {

        if (pst > pend || inst > inend) { // BASE CASE
            return null;
        }

        int d = preorder[pst];

        BtNode nnode = new BtNode(d);

        // NOTE: Find Current Preorderdata in inorderlist
        int offset = inst;
        for (; offset < inend; offset++) {
            if (inorder[offset] == d) {
                break;
            }
        }

        nnode.left = buildBttree(preorder, pst + 1, pst + offset - inst, inorder, inst, offset - 1);
        nnode.right = buildBttree(preorder, pst + offset - inst + 1, pend, inorder, offset + 1, inend);

        return nnode;
    }
    // #endregion

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Root Created with DATA = 1");
        BtNode root = new BtNode(1);
        int t = 1;
        while (t == 1) {
            System.out.println(
                    "1.Insert  2. Levelorder Traversal 3.Size 4.Height 5.DELETE 6.Maxwidth 7.Inorder-Preorder");
            int ch = sc.nextInt();

            switch (ch) {
            case 1:
                System.out.println("Enter the Data");
                int d = sc.nextInt();
                TreeDS.insertElement(root, d);
                break;
            case 2:
                System.out.println("Levelorder traversal");
                levelordertraversal(root);
                System.out.println();
                break;
            case 3:
                System.out.println("Size of Tree = " + sizeoftree(root));
                break;
            case 4:
                System.out.println("Depth/Height of Tree = " + depthoftree(root));
                break;
            case 5:
                System.out.println("Deleting Node from Tree with 2 ");
                deletenode(root, 2);
                System.out.println("Inorder traversal");
                inorder(root);
                break;
            case 6:
                System.out.println("Max width of tree is = " + maxwidth(root));
                break;
            case 7:
                System.out.println("Enter number of elements");
                int last = sc.nextInt();
                System.out.println("Enter Inorder then Preorder traversal of tree");
                int[] inorder = new int[last];
                int[] preorder = new int[last];
                for (int i = 0; i < last; i++) {
                    inorder[i] = sc.nextInt();
                }
                System.out.println("Enter Preorder");
                for (int i = 0; i < last; i++) {
                    preorder[i] = sc.nextInt();
                }
                root = buildBttree(preorder, 0, preorder.length - 1, inorder, 0, inorder.length - 1);
                System.out.println("Levelorder traversal");
                levelordertraversal(root);
                System.out.println();
                break;
            default:
                break;
            }
            System.out.println("Want More Operations ? Enter 1 or 0");
            t = sc.nextInt();
        }
    }
}

class BtNode {
    int data;
    BtNode left, right;

    BtNode(int x) {
        this.data = x;
        this.left = null;
        this.right = null;
    }

    public int getData() {
        return data;
    }

    public BtNode getLeft() {
        return left;
    }

    public BtNode getRight() {
        return right;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setLeft(BtNode left) {
        this.left = left;
    }

    public void setRight(BtNode right) {
        this.right = right;
    }
}