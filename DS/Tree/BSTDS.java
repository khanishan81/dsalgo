import java.util.*;

class BSTDS {

    public static BstNode insertbst(BstNode r, int d) {
        if (r == null) {
            BstNode newnode = new BstNode(d);
            r = newnode;
            return newnode;
        }

        if (d < r.data) {
            r.left = insertbst(r.left, d);
        } else if (d > r.data) {
            r.right = insertbst(r.right, d);
        }
        return r;
    }

    public static String searchbst(BstNode r, int data) {

        if (r == null) {
            return "Not Found";
        } else if (r.data == data) {
            return "Found";
        } else if (data > r.data) {
            return searchbst(r.right, data);
        } else if (data < r.data) {
            return searchbst(r.left, data);
        } else {
            return "Not Found";
        }
    }

    public static void levelordertraversal(BstNode r) {
        Queue<BstNode> q = new LinkedList<BstNode>();
        q.add(r);
        while (!q.isEmpty()) {
            BstNode curr = q.poll();
            System.out.print(curr.data + " ");
            if (curr.left != null) {
                q.add(curr.left);
            }
            if (curr.right != null) {
                q.add(curr.right);
            }
        }
    }

    // #region [Color1] Deletion of a node
    public static int minValue(BstNode root) { // find the minimum value in right ST.
        int minv = root.data;
        while (root.left != null) {
            minv = root.left.data;
            root = root.left;
        }
        return minv;
    }

    public static BstNode DeleteNode(BstNode root, int data) {

        if (root == null) {
            return root;
        }
        if (data > root.data) { // Reccursion
            root.right = DeleteNode(root.right, data);
            return root;
        }
        if (data < root.data) {
            root.left = DeleteNode(root.left, data);
            return root;
        } else { // Found
            if (root.left == null && root.right == null) {// Node with no child.
                root = null;
                return root;
            } else if (root.left == null) { // Node with both Child
                root = root.right;
                return root;
            } else if (root.right == null) {
                root = root.left;
                return root;
            } else { // Node with both child
                root.data = minValue(root.right);
                root.right = DeleteNode(root.right, root.data);
            }
        }
        return root;
    }
    // #endregion

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BstNode root = null;

        int t = 1;
        while (t == 1) {
            System.out.println("1.Insert 2.Search 3.traversal 4.Delete");
            int ch = sc.nextInt();

            switch (ch) {
            case 1:
                System.out.println("Enter Data to insert ");
                int data = sc.nextInt();
                root = insertbst(root, data);
                break;
            case 2:
                System.out.println("Enter key to search");
                int k = sc.nextInt();
                System.out.println(searchbst(root, k));
                break;
            case 3:
                System.out.println("Level Order traversal");
                levelordertraversal(root);
                System.out.println();
                break;
            case 4:
                System.out.println("Enter the data to delete");
                int temp = sc.nextInt();
                root = DeleteNode(root, temp);
                break;
            default:
                break;
            }
            System.out.println("Continue 0 OR 1 ");
            t = sc.nextInt();
        }
    }
}

class BstNode {
    int data;
    BstNode left;
    BstNode right;

    BstNode(int x) {
        this.data = x;
        this.left = null;
        this.right = null;
    }

    public int getData() {
        return data;
    }

    public BtNode getLeft() {
        return left;
    }

    public BtNode getRight() {
        return right;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setLeft(BstNode left) {
        this.left = left;
    }

    public void setRight(BstNode right) {
        this.right = right;
    }
}