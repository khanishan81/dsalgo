import java.util.*;

public class Solution {
     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          Queue<student> queue = new LinkedList();
          List<student> l;

          int t = sc.nextInt();

          while (t-- > 0) {
               String ch = sc.next();
               if (ch.equals("E")) {
                    int a = sc.nextInt();
                    int b = sc.nextInt();

                    student obj = new student(a, b);
                    l = new ArrayList<student>(queue);
                    int pos = findpos(l, obj);
                    if (pos < 0) {
                         queue.add(obj);
                    } else if (pos == 0 && queue.isEmpty()) {
                         queue.add(obj);
                    } else {
                         l = insert(l, pos, obj);

                         while (!queue.isEmpty()) {
                              queue.remove();
                         }
                         int i = 0;
                         while (i < l.size()) {
                              queue.add(l.get(i));
                              i++;
                         }
                    }

               } else {
                    student removedobj = queue.poll();
                    System.out.println(removedobj.school + " " + removedobj.rollno);

               }
          }
     }

     public static int findpos(List l, student o) {
          // o.school= 55;
          int len = l.size();
          if (len <= 0) {
               return 0;
          } else {
               int i;
               for (i = l.size() - 1; i >= 0;) {
                    student objtemp = (student) l.get(i);
                    if (objtemp.school == o.school) {
                         return i;
                    } else {
                         i--;
                    }
               }
               if (i == 0) {
                    return 0;
               }
               if (i < 0) {
                    return l.size() - 1;
               }
               return 0;
          }
     }

     public static List insert(List l, int pos, student obj) {
          if (pos == l.size() - 1) {
               l.add(obj);
               return l;
          } else {
               List<student> temp2 = new ArrayList<student>();
               for (int i = 0; i <= l.size(); i++) {
                    if (i >= pos + 1) {
                         if (i == pos + 1) {
                              temp2.add(obj);
                              // temp2.add((student) l.get(i));
                         } else {
                              if (i == 0) {
                                   temp2.add((student) l.get(i));
                              } else {
                                   temp2.add((student) l.get(i - 1));
                              }
                         }
                    } else {

                         temp2.add((student) l.get(i));
                    }
               }
               return temp2;
          }
          // return l;

     }
}

class student {
     int school;
     int rollno;

     public student(int x, int y) {
          this.school = x;
          this.rollno = y;
     }
}