import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
 
import java.util.*;
 
 //  FIXME: Make custom solution. 
class TestClass {
    public static void main(String args[] ) throws Exception {
	Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
 
		Queue<Integer> queue[] = new Queue[t + 1];
		for (int i = 0; i <= t; i++)
			queue[i] = new LinkedList<Integer>();
 
		Queue<Integer> que1 = new LinkedList<>();
 
		while (t-- > 0) {
			String ch = sc.next();
			if (ch.equals("E")) {
				int a = sc.nextInt();
				int b = sc.nextInt();
				if (queue[a].isEmpty()) {
					que1.offer(a);
				}
					queue[a].offer(b);
				}
			 else {
					int temp = que1.peek();
					System.out.println(temp + " "+queue[temp].peek());
					queue[temp].poll();
					if(queue[que1.peek()].isEmpty()) {
						que1.poll();
					}
			}
		}
 
    }
}

/* 5
E 1 1
E 2 1
E 1 2
D
D 

--- 2 
E 2 1
E 2 2
D
E 1 1
D
D 

2 1
2 2
1 1

*/