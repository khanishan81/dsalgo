import java.util.*;
import java.util.StringTokenizer;
import java.lang.*;
import java.io.*;

class Primality {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());

        while (t-- > 0) {
            long num = Long.parseLong(br.readLine());
            System.out.println(Primality.primalitytest(num));

        }
    }

    public static String primalitytest(long num) {
        if (num == 1) {
            return "composite";
        }
        if (num % 2 == 0 && num != 2) {
            return "composite";
        }
        for (long i = 3; i <= Math.sqrt(num); ++i) {
            if (num % i == 0) {
                return "composite";
            }
        }

        return "prime";
    }

}
