
// TODO: https://leetcode.com/problems/complex-number-multiplication/
// BUG: works only for One Digit Numbers. FIXME: 

import java.util.*;

public class Complexnumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1 = sc.nextLine();
        String s2 = sc.nextLine();
        String ans = complexNumberMultiply(s1, s2);
        System.out.println(ans);
    }

    public static String complexNumberMultiply(String a, String b) {
        int num1[] = new int[5];
        int num2[] = new int[5];
        int ai = 0;
        String ans;

        for (int i = 0; i < a.length(); i++) {
            char cur = a.charAt(i);
            if (cur == ' ' || cur == 'i' || cur == '+') {
            } else {
                if (i > 0 && cur == '-') {
                    char cnext = a.charAt(++i);
                    String nxt = String.valueOf(cur) + String.valueOf(cnext);
                    num1[ai] = Integer.parseInt(nxt);
                    break;
                } else {
                    num1[ai] = Integer.parseInt(String.valueOf(cur));
                    ai++;
                }
            }
        }
        ai = 0;
        for (int i = 0; i < b.length(); i++) {
            char cur = b.charAt(i);
            if (cur == ' ' || cur == 'i' || cur == '+') {
            } else {
                if (i > 0 && cur == '-') {
                    char cnext = b.charAt(++i);
                    String nxt = String.valueOf(cur) + String.valueOf(cnext);
                    num2[ai] = Integer.parseInt(nxt);
                    break;
                } else {
                    num2[ai] = Integer.parseInt(String.valueOf(cur));
                    ai++;
                }
            }
        }
        int ans1 = (num1[0] * num2[0]) - (num1[1] * num2[1]);
        int ans2 = (num1[0] * num2[1]) + (num1[1] * num2[0]);
        ans = String.valueOf(ans1);
        ans = ans + "+";
        ans += String.valueOf(ans2);
        ans += String.valueOf('i');
        return ans;
    }
}