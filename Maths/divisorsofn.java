import java.util.*;

class divisorfon {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        while (t-- > 0) {
            int n = sc.nextInt();

            int cnt = 0;
            for (int i = 1; i * i <= n; i++) {
                if (n % i == 0) {
                    if (i * i == n) {
                        cnt++;
                    } else {
                        cnt = cnt + 2;
                    }
                }
            }
            System.out.println(cnt); // result.
        }
    }
}