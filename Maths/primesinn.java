import java.util.*;

public class primesinn {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        boolean isPrime[] = new boolean[n + 1];

        for (int i = 0; i < n + 1; i++) {
            isPrime[i] = true;
        }

        isPrime[0] = false;
        isPrime[1] = false;

        for (int i = 2; i * i <= n + 1; ++i) {
            if (isPrime[i] == true) { // Mark all the multiples of i as composite numbers
                for (int j = i * i; j <= n; j += i)
                    isPrime[j] = false;
            }
        }
        int cnt = 0;
        for (int i = 0; i < n + 1; i++) {
            if (isPrime[i])
                cnt++;
        }
        System.out.println(cnt);

    }
}