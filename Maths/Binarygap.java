
// TODO: https://leetcode.com/problems/binary-gap/ 
import java.util.*;

public class Binarygap {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(binaryGap(n));
    }

    public static int binaryGap(int N) {
        int max = 0;
        int d = -32;
        while (N > 0) {
            if (N % 2 == 1) {
                max = Math.max(max, d);
                d = 0;
            }
            N /= 2;
            d++;
        }
        return max;
    }
}