import java.util.*;

public class productofsum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        while (t-- > 0) {
            long mod = 1000000007;
            long ans = 0;
            long last;
            long n = sc.nextLong();

            long i;
            for (i = 1; i * i <= n; ++i)
                ans = (ans + (i * (n / i)) % mod) % mod;
            --i;
            last = n / i;
            for (i = 1; i < last; ++i) {
                long en = n / i;
                long st = n / (i + 1);
                long diff = (en * (en + 1) / 2) - (st * (st + 1) / 2);
                ans = (ans + (i * diff) % mod) % mod;

            }
            System.out.println(ans);
        }
    }
}