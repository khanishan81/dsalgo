
// TODO: https://www.hackerrank.com/challenges/primitive-problem/problem
import java.util.*;

public class primitiveroots {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        // Step1 find primefactors
        ArrayList<Integer> facs = new ArrayList<Integer>();
        findprimefacs(n, facs);

        // Step2 Find primitive roots usig power method
        int primit = findprimitve(n, facs);
        // Step3 find find number of primitive roots.
        int numroots = numberofprimitiveroots(n - 1, facs);

        System.out.println(primit + " " + numroots);

    }

    public static void findprimefacs(int n, ArrayList<Integer> facs) {
        int num = n - 1;
        if (num % 2 == 0) {
            facs.add(2);
            while (num % 2 == 0) {
                num = num / 2;
            }
        }

        for (int i = 3; i < Math.sqrt(12) + 1; i = i + 2) {
            if (num % i == 0) {
                facs.add(i);
                while (num % i == 0) {
                    num = num / 2;
                }
            }
        }
        if (num > 2) {
            facs.add(num);
        }
    }

    // Iterative Function to calculate (x^n)%p in O(logy)
    public static int power(int x, int y, int p) {
        int res = 1; // Initialize result

        x = x % p; // Update x if it is more than or
        // equal to p

        while (y > 0) {
            // If y is odd, multiply x with result
            if (y % 2 != 0)
                res = (res * x) % p;

            // y must be even now
            y = y / 2; // y = y/2
            x = (x * x) % p;
        }
        return res;
    }

    public static int findprimitve(int n, ArrayList<Integer> facs) {
        int phi = n - 1;
        // Check for every number from 2 to phi
        for (int r = 2; r <= phi; r++) {
            // Iterate through all prime factors of phi.
            // and check if we found a power with value 1
            boolean flag = false;
            for (int it : facs) {

                // Check if r^((phi)/primefactors) mod n
                // is 1 or not
                if (power(r, phi / (it), n) == 1) {
                    flag = true;
                    break;
                }
            }

            // If there was no power with value 1.
            if (flag == false)
                return r;
        }
        return -1;
    }

    public static int numberofprimitiveroots(int n, ArrayList<Integer> facs) {
        int result = 1;
        for (int i = 2; i < n; i++)
            if (__gcd(i, n) == 1)
                result++;

        return result;
    }

    public static int __gcd(int a, int b) {
        // Everything divides 0
        if (a == 0)
            return b;
        if (b == 0)
            return a;

        // base case
        if (a == b)
            return a;

        // a is greater
        if (a > b)
            return __gcd(a - b, b);
        return __gcd(a, b - a);
    }

}