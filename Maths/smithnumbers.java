import java.util.*;
import java.lang.*;

public class smithnumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        long n = sc.nextLong();

        long dg = sumdigit(n);

        long fg = facsums(n);

        if (dg == fg) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }

    }

    public static long sumdigit(long num) {
        long sum = 0;
        while (num > 0) {
            sum = sum + (num % 10);
            num = num / 10;
        }
        return sum;
    }

    public static long facsums(long n) {
        long fsum = 0;
        long num = n;
        while (n % 2 == 0) {

            fsum = fsum + 2;
            n = n / 2;

        }
        for (long j = 3; j <= Math.sqrt(n); j = j + 2) {
            while (n % j == 0) {
                if (j >= 10) {
                    long p = j;
                    while (p != 0) {
                        long r = p % 10;
                        fsum = fsum + r;
                        p = p / 10;
                    }
                } else {
                    fsum = fsum + j;
                }
                n = n / j;
            }
        }
        if (n > 1) {
            while (n != 0) {
                long r = n % 10;
                fsum = fsum + r;
                n = n / 10;
            }

        }
        return fsum;
    }
}