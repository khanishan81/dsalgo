public class modexponent {
    public static void main(String[] args) {
        System.out.println("Hello world");
        System.out.println(iterativemodularExponentiation(2, 5, 13));
    }

    public static int modularExponentiation(int x, int n, int M) {
        if (n == 0)
            return 1;
        else if (n % 2 == 0) // n is even
            return modularExponentiation((x * x) % M, n / 2, M);
        else // n is odd
            return (x * modularExponentiation((x * x) % M, (n - 1) / 2, M)) % M;

    }

    public static int iterativemodularExponentiation(int x, int n, int M) {
        int result = 1;
        while (n > 0) {
            if (n % 2 == 1)
                result = (result * x) % M;
            x = (x * x) % M;
            n = n / 2;
        }
        return result;
    }
}